﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Satovi.Models;

namespace Satovi.Controllers
{
    public class ProdavnicaController : Controller
    {
        public ActionResult Index()
        {
            List<Prodavnica> stores = (List<Prodavnica>)HttpContext.Application["stores"];
            return View(stores);
        }
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Prodavnica prodavnica)
        {
            if (!ModelState.IsValid)
                return View("Error");
            List<Prodavnica> stores = (List<Prodavnica>)HttpContext.Application["stores"];

            stores.Add(prodavnica);
            HttpContext.Application["stores"] = stores;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(string name, string address)
        {
            List<Prodavnica> stores = (List<Prodavnica>)HttpContext.Application["stores"];

            foreach (var item in stores)
            {
                if (item.Name.Equals(name) && item.Address.Equals(address))
                {
                    item.IsDeleted = true;
                    foreach (var clock in item.Clocks)
                    {
                        clock.IsDeleted = true;
                    }
                    break;
                }
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Items(string name, string address)
        {
            List<Prodavnica> stores = (List<Prodavnica>)HttpContext.Application["stores"];

            foreach (var item in stores)
            {
                if (item.Name.Equals(name) && item.Address.Equals(address))
                {
                    HttpContext.Session["storeSpecific"] = item.Clocks;
                    return RedirectToAction("Index", "Sat");
                }
            }
            return View("Error");
        }
    }
}