﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Satovi.Models;

namespace Satovi.Controllers
{
    public class KupovinaController : Controller
    {
        public ActionResult Index()
        {
            List<Kupovina> purchases = (List<Kupovina>)HttpContext.Application["purchases"];
            List<Kupovina> temp = new List<Kupovina>();
            foreach (var item in purchases)
            {
                if (item.User.Username.Equals(((Korisnik)HttpContext.Session["user"]).Username))
                    temp.Add(item);
            }
            return View(temp);
        }
    }
}