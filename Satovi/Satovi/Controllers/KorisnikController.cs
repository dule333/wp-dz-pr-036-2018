﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Satovi.Models;

namespace Satovi.Controllers
{
    public class KorisnikController : Controller
    {
        public ActionResult Index()
        {
            HttpContext.Session["storeSpecific"] = null;
            return View();
        }
        public ActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Korisnik user)
        {
            if (!ModelState.IsValid)
                return View("Error");
            List<Korisnik> users = (List<Korisnik>)HttpContext.Application["users"];
            
            foreach(var temp in users)
            { 
                if (temp.Username.Equals(user.Username)) return View("Error");
            }
            foreach (char item in user.Password)
            {
                if (!Char.IsLetterOrDigit(item)) return View("Error");
            }
            
            users.Add(user);
            HttpContext.Application["users"] = users;
            return RedirectToAction("Login");
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            HttpContext.Session["admin"] = false;
            HttpContext.Session["loggedIn"] = false;
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Login(Korisnik user)
        {
            List<Korisnik> users = (List<Korisnik>)HttpContext.Application["users"];
            foreach (var temp in users)
                if (temp.Username.Equals(user.Username) && temp.Password.Equals(user.Password) && !temp.IsDeleted)
                {
                    if (temp.IsAdmin) HttpContext.Session["admin"] = true;
                    else HttpContext.Session["admin"] = false;
                    HttpContext.Session["loggedIn"] = true;
                    HttpContext.Session["purchases"] = new List<Kupovina>();
                    HttpContext.Session["basket"] = new List<Sat>();
                    HttpContext.Session["basketTotal"] = 0;
                    HttpContext.Session["user"] = user;
                    return RedirectToAction("../Sat/Index");
                }

            return View("Error");
        }
        public ActionResult List()
        {
            List<Korisnik> users = (List<Korisnik>)HttpContext.Application["users"];
            return View(users);
        }
        [HttpPost]
        public ActionResult List(string username)
        {
            List<Korisnik> users = (List<Korisnik>)HttpContext.Application["users"];
            foreach(var user in users)
            {
                if (user.Username == username)
                {
                    user.IsDeleted = true;
                    break;
                }
            }
            HttpContext.Application["users"] = users;
            return View(users);
        }
        [HttpPost]
        public ActionResult BasketIt(string clock)
        {
            List<Sat> clocks = (List<Sat>)HttpContext.Application["clocks"];
            List<Sat> basket = (List<Sat>)HttpContext.Session["basket"];
            foreach (var item in clocks)
            {
                if(item.Name.Equals(clock) && !item.IsDeleted && item.OnSale && !basket.Contains(item))
                {
                    basket.Add(item);
                    HttpContext.Session["basket"] = basket;
                    int temp = Convert.ToInt32(item.Price) + (int)HttpContext.Session["basketTotal"];
                    HttpContext.Session["basketTotal"] = temp;
                }
            }
            return RedirectToAction("Index", "Sat");
        }
        public ActionResult Basket()
        {
            return View((List<Sat>)HttpContext.Session["basket"]);
        }
        [HttpPost]
        public ActionResult Remove(string name)
        {
            List<Sat> basket = (List<Sat>)HttpContext.Session["basket"];
            Sat itemToRemove = null;
            foreach (var item in basket)
            {
                if(item.Name.Equals(name))
                {
                    int temp = (int)HttpContext.Session["basketTotal"] - Convert.ToInt32(item.Price);
                    HttpContext.Session["basketTotal"] = temp;
                    itemToRemove = item;
                    break;
                }
            }
            basket.Remove(itemToRemove);
            HttpContext.Session["basket"] = basket;
            return RedirectToAction("Basket");
        }

        [HttpPost]
        public ActionResult Purchase()
        {
            List<Sat> basket = (List<Sat>)HttpContext.Session["basket"];
            List<Sat> clocks = (List<Sat>)HttpContext.Application["clocks"];
            foreach (var item in basket)
            {
                if(!clocks.Contains(item))
                {
                    return View("Error");
                }
            }
            List<Sat> tempClocks = new List<Sat>();
            foreach (var item in basket)
            {
                clocks[clocks.IndexOf(item)].OnSale = false;
                tempClocks.Add(item);
            }

            Kupovina purchase = new Kupovina((Korisnik)HttpContext.Session["user"], tempClocks);
            ((List<Kupovina>)HttpContext.Application["purchases"]).Add(purchase);
            return RedirectToAction("Index", "Kupovina");
        }
    }
}