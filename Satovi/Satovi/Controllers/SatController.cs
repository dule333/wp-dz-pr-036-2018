﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Satovi.Models;

namespace Satovi.Controllers
{
    public class SatController : Controller
    {
        public ActionResult Index()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            return View(clocks);
        }
        public ActionResult SortAName()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            clocks.Sort(delegate (Sat x, Sat y)
            {
                if (string.Compare(x.Name, y.Name) >= 0) return 1;
                else return -1;
            });
            return View("Index", clocks);
        }
        public ActionResult SortDName()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            clocks.Sort(delegate (Sat x, Sat y)
            {
                if (string.Compare(x.Name, y.Name) >= 0) return -1;
                else return 1;
            });
            return View("Index", clocks);
        }
        public ActionResult SortAMan()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            clocks.Sort(delegate (Sat x, Sat y)
            {
                if (string.Compare(x.Manufacturer, y.Manufacturer) >= 0) return 1;
                else return -1;
            });
            return View("Index", clocks);
        }
        public ActionResult SortDMan()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            clocks.Sort(delegate (Sat x, Sat y)
            {
                if (string.Compare(x.Manufacturer, y.Manufacturer) >= 0) return -1;
                else return 1;
            });
            return View("Index", clocks);
        }
        public ActionResult SortAPrice()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            clocks.Sort(delegate (Sat x, Sat y)
            {
                if (x.Price > y.Price) return 1;
                else return -1;
            });
            return View("Index", clocks);
        }
        public ActionResult SortDPrice()
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            clocks.Sort(delegate (Sat x, Sat y)
            {
                if (x.Price > y.Price) return -1;
                else return 1;
            });
            return View("Index", clocks);
        }
        [HttpPost]
        public ActionResult SearchByName(string value)
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            List<Sat> newList = new List<Sat>();
            foreach (var item in clocks)
            {
                if (item.Name.Contains(value))
                    newList.Add(item);
            }
            return View("Index", newList);
        }
        [HttpPost]
        public ActionResult SearchByMan(string value)
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            List<Sat> newList = new List<Sat>();
            foreach (var item in clocks)
            {
                if (item.Manufacturer.Contains(value))
                    newList.Add(item);
            }
            return View("Index", newList);
        }
        [HttpPost]
        public ActionResult SearchByPrice(int value, int value2)
        {
            List<Sat> clocks = (HttpContext.Session["storeSpecific"] != null)? (List<Sat>)HttpContext.Session["storeSpecific"] : (List<Sat>)HttpContext.Application["clocks"];
            List<Sat> newList = new List<Sat>();
            if (value2 == 0) value2 = 400000;
            foreach (var item in clocks)
            {
                if (item.Price >= value && item.Price <= value2)
                    newList.Add(item);
            }
            return View("Index", newList);

        }
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(FormCollection collection)
        {
            Sat clock = new Sat(collection["name"], collection["manufacturer"], collection["color"], collection["material"], collection["description"], uint.Parse(collection["price"]),
                ((collection["onsale"] != null && collection["onsale"].Equals("on")) ? true : false));
            if (clock.Name.Length < 1) return View("Error");
            if (clock.Price < 0 || clock.Price > 400000) return View("Error");
            List<Sat> clocks = (List<Sat>)HttpContext.Application["clocks"];
            List<Prodavnica> stores = (List<Prodavnica>)HttpContext.Application["stores"];

            foreach (var temp in stores)
            {
                if (temp.Name.Equals(collection["store"]))
                {
                    clocks.Add(clock);
                    temp.Clocks.Add(clock);
                    HttpContext.Application["clocks"] = clocks;
                    return RedirectToAction("Index");
                }
            }
            return View("Error");
        }
        [HttpPost]
        public ActionResult Modify(string clock)
        {
            List<Sat> clocks = (List<Sat>)HttpContext.Application["clocks"];
            foreach (var item in clocks)
            {
                if (item.Name.Equals(clock))
                {
                    HttpContext.Session["ModifiedClock"] = item;
                    return View("Modify", item);
                }
            }
            return View("Error");
        }
        [HttpPost]
        public ActionResult ModifyExecute(Sat clock)
        {
            if (!ModelState.IsValid)
                return View("Error");

            List<Sat> clocks = (List<Sat>)HttpContext.Application["clocks"];
            Sat oldClock = (Sat)HttpContext.Session["ModifiedClock"];
            clocks[clocks.IndexOf(oldClock)] = clock;

            return RedirectToAction("~/Sat/Index");
        }

    }
}