using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Satovi.Models;

namespace Satovi
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            CultureInfo newCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            newCulture.DateTimeFormat.LongDatePattern = "dd/MM/yyyy HH:mm:ss";
            newCulture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = newCulture;

            string userPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Users.txt";
            string clockPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Clocks.txt";
            string storePath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Stores.txt";

            using (StreamReader streamReader = new StreamReader(userPath))
            {
                List<Korisnik> tempList = new List<Korisnik>();
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    tempList.Add(new Korisnik(splitted[0], splitted[1], splitted[2], splitted[3], splitted[4], splitted[5], DateTime.Parse(splitted[6]), bool.Parse(splitted[7]), bool.Parse(splitted[8])));
                    temp = streamReader.ReadLine();
                }
                Application["users"] = tempList;
            }

            using (StreamReader streamReader = new StreamReader(clockPath))
            {
                List<Sat> tempList = new List<Sat>();
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    tempList.Add(new Sat(splitted[0], splitted[1], splitted[2], splitted[3], splitted[4], uint.Parse(splitted[5]), bool.Parse(splitted[6])));
                    temp = streamReader.ReadLine();
                }
                Application["clocks"] = tempList;
            }

            using (StreamReader streamReader = new StreamReader(storePath))
            {
                List<Prodavnica> tempList = new List<Prodavnica>();
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    List<Sat> clocks = new List<Sat>();
                    foreach (var item in (List<Sat>)Application["clocks"])
                    {
                        clocks.Add(item);
                    }
                    string[] splitted = temp.Split(';');
                    tempList.Add(new Prodavnica(splitted[0], splitted[1], clocks, bool.Parse(splitted[2])));
                    temp = streamReader.ReadLine();
                }
                Application["stores"] = tempList;
            }

            Application["purchases"] = new List<Kupovina>();
        }
    }
}
