﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Satovi.Models
{
    public class Kupovina
    {
        public Korisnik User
        { get; set; }
        public List<Sat> Clocks
        { get; set; }
        public DateTime TimeOfPurchase
        { get; set; }
        public int PurchaseTotal
        { get; set; }
        public bool IsDeleted
        { get; set; }

        public Kupovina(Korisnik user, List<Sat> clocks)
        {
            User = user;
            Clocks = clocks;
            TimeOfPurchase = DateTime.Now;
            int purchaseTotal = 0;
            foreach (var clock in Clocks)
            {
                purchaseTotal += Convert.ToInt32(clock.Price);
            }
            PurchaseTotal = purchaseTotal;
            IsDeleted = false;
        }
    }
}