﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Satovi.Models
{
    public class Korisnik
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Username
        { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 8)]
        public string Password
        { get; set; }
        public string Name
        { get; set; }
        public string Surname
        { get; set; }
        public string Gender
        { get; set; }
        public string Email
        { get; set; }
        public DateTime DateOfBirth
        { get; set; }
        public bool IsAdmin
        { get; set; }
        public bool IsDeleted
        { get; set; }

        public Korisnik(string username, string password, string name, string surname, string gender, string email, DateTime dateOfBirth, bool isAdmin, bool isDeleted)
        {
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Gender = gender;
            Email = email;
            DateOfBirth = dateOfBirth;
            IsAdmin = isAdmin;
            IsDeleted = isDeleted;
        }
        public Korisnik()
        {
            IsDeleted = false;
            IsAdmin = false;
        }
    }
}