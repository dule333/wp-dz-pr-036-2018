﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Satovi.Models
{
    public class Prodavnica
    {
        [StringLength(50, MinimumLength = 1)]
        public string Name
        { get; set; }
        public string Address
        { get; set; }
        public List<Sat> Clocks
        { get; set; }
        public bool IsDeleted
        { get; set; }

        public Prodavnica(string name, string address, List<Sat> clocks, bool isDeleted)
        {
            Name = name;
            Address = address;
            Clocks = clocks;
            IsDeleted = isDeleted;
        }
        public Prodavnica()
        {
            Clocks = new List<Sat>();
            IsDeleted = false;
        }
    }
}