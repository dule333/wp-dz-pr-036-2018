﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Satovi.Models
{
    public class Sat
    {
        [StringLength(50, MinimumLength = 1)]
        public string Name
        { get; set; }
        public string Manufacturer
        { get; set; }
        public string Color
        { get; set; }
        public string Material
        { get; set; }
        public string Description
        { get; set; }
        [Range(0,400000)]
        public uint Price
        { get; set; }
        public bool OnSale
        { get; set; }
        public bool IsDeleted
        { get; set; }

        public Sat(string name, string manufacturer, string color, string material, string description, uint price, bool onSale)
        {
            Name = name;
            Manufacturer = manufacturer;
            Color = color;
            Material = material;
            Description = description;
            Price = price;
            OnSale = onSale;
            IsDeleted = false;
        }
        public Sat()
        {
            IsDeleted = false;
        }
    }
}